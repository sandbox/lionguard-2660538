<?php

/**
 * @file
 */

/**
 * Implements hook_field_views_data_alter().
 */
function addressfield_email_field_views_data_alter(&$result, $field, $module) {
  if ($module == 'addressfield') {
    foreach ($result as $table_name => $table) {
      $field_name = $field['field_name'];
      if (isset($result[$table_name][$field_name])) {
        $field_title = $result[$table_name][$field_name]['title'];
        $group = $result[$table_name][$field_name]['group'];
        $title = $field_title . ' with email';
        $help = t('!data Address field email from !fieldname field', array('!data' => $result[$table_name][$field_name . '_data']['help'], '!fieldname' => $field_name));
        $result[$table_name]['table']['group'] = t('Address emails');

        $result[$table_name][$field_name . '_data'] = array(
          'group' => $group,
          'title' => $title,
          'help' => $help,
          'field' => array(
            'handler' => 'addressfield_email_handler_field_address_email_nr',
            'click sortable' => TRUE,
          ),
        );
      }
    }
  }
}
