<?php

/**
 * @file
 * Definition of addressfield_email_handler_field_address_email_nr.
 */

/**
 * Provides different email number display options for address entity(field).
 *
 * @ingroup views_field_handlers
 */
class addressfield_email_handler_field_address_email_nr extends views_handler_field {

  /**
   * Option to address email field.
   */
  public function option_definition() {
    $options = parent::option_definition();
    $options['selected_email_type'] = array('default' => 3, 'bool' => TRUE);
    return $options;
  }

  /**
   * Link to Addressfield email field option form.
   */
  public function options_form(&$form, &$form_state) {
    $form['selected_email_type'] = array(
      '#type' => 'select',
      '#title' => t('Email type'),
      '#options' => array(
        1 => t('Email'),
      ),
      '#default_value' => $this->options['selected_email_type'],
      '#description' => t('Address field contains an email - select a type.'),
    );

    parent::options_form($form, $form_state);
  }

  /**
   * Render the addresfields email field.
   */
  public function render($values) {
    $emailValues = unserialize($this->get_value($values));
    $email = $emailValues['email'];
    switch ($this->options['selected_email_type']) {
      case 1:
        return $this->sanitize_value($email);

      break;
    }
  }

}
