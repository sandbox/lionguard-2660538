CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation & Configuration


INTRODUCTION
------------
Address Field Email expands the functionality of the addressfield module by enabling the collection of an email address within an address field.

This module was created specifically for the scenario where a multi-value address field is being used to store information about multiple individuals, so each individual's email address could be referenced separately.

It uses Drupal core's email validation, and therefore only has the address field module as a dependency. The code for this module is mostly based on addressfield_phone, which enables the collection of phone/fax related information in an address field.

REQUIREMENTS
------------
Address Field Email requires the address field module ( https://www.drupal.org/project/addressfield )

INSTALLATION & CONFIGURATION
----------------------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
* When creating a new address field, enable the capture of email in the address field configuration
