<?php

/**
 * @file
 * The default format for addresses.
 */

$plugin = array(
  'title' => t('Email address'),
  'format callback' => 'addressfield_email_format_email_generate',
  'type' => 'email',
  'weight' => -99,
);

/**
 * Format callback.
 *
 * @see CALLBACK_addressfield_format_callback()
 */
function addressfield_email_format_email_generate(&$format, $address, $context = array()) {
  if ($context['mode'] == 'form' && !empty($address['country'])) {
    $format['email_block'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('custom-block')),
      '#weight' => 200,
    );
    $format['email_block']['email'] = array(
      '#type' => 'addressfield_container',
      '#attributes' => array('class' => array('addressfield-container')),
			'#element_validate' => array('_addressfield_email_block_validate'),
    );

    if (isset($context['instance']['widget']['settings']['email_fields'])) {
      $settings = array_filter($context['instance']['widget']['settings']['email_fields']);
    }

    if (empty($settings) || !empty($settings['email'])) {
      $format['email_block']['email']['email'] = array(
        '#title' => t('Contact email'),
        '#size' => 15,
        '#attributes' => array('class' => array('email-number')),
        '#type' => 'textfield',
        '#tag' => 'span',
        '#default_value' => isset($address['email']) ? $address['email'] : '',
        '#element_validate' => array('_addressfield_email_validate'),
        '#required' => isset($settings['required']) && $settings['required'] == 1 ? TRUE : FALSE,
        '#parent_field' => $context['field']['field_name'],
        '#unique' => $context['instance']['widget']['settings']['email_fields']['unique'],
      );
			if(isset($settings['confirmation']) && $settings['confirmation'] == 1){
				$format['email_block']['email']['email_confirmation'] = array(
					'#title' => t('Email confirmation'),
					'#description' => 'Please enter email again',
					'#size' => 15,
					'#attributes' => array('class' => array('email-number')),
					'#type' => 'textfield',
					'#tag' => 'span',
					'#default_value' => isset($address['email']) ? $address['email'] : '',
					'#element_validate' => array('_addressfield_email_validate'),
					'#required' => isset($settings['required']) && $settings['required'] == 1 ? TRUE : FALSE,
					'#parent_field' => $context['field']['field_name'],
				);				
			}
    }
    else {
      // Add our own render callback for the format view.
      $format['#pre_render'][] = '_addressfield_email_render_address';
    }
  }
}
